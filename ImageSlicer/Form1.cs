﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageSlicer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Image img = Image.FromFile( FileName.Text );

            int iTopOffset = Convert.ToInt32(sTopOffset.Text);
            int iLeftOffset = Convert.ToInt32(sLeftOffset.Text);
            int iXStep = Convert.ToInt32(XStep.Text);
            int iYStep = Convert.ToInt32(YStep.Text);
            int iWidth = Convert.ToInt32(sWidth.Text);
            int iHeight = Convert.ToInt32(sHeight.Text);

            int nTilesWide = (img.Width - iLeftOffset)/(iWidth*iXStep);
            int nTilesHigh = (img.Height - iTopOffset)/(iHeight*iYStep);

            Bitmap[] imgarray = new Bitmap[nTilesWide*nTilesHigh];

            int t = 0;
            int n = 0;
            for( int y=0;y<nTilesHigh;y++)
            for( int x=0;x<nTilesWide;x++ )            
            {
                n++;
                int x0 = iLeftOffset+(x*(iWidth*iXStep));
                int y0 = iTopOffset+(y*(iHeight*iYStep));
                int x1 = x0+(iWidth*iXStep);
                int y1 = y0+(iHeight*iYStep);

                PointF S = new PointF(x0, y0);
                PointF E = new PointF(x1,y1);

                imgarray[t] = new Bitmap(iWidth,iHeight);
                Graphics ic = Graphics.FromImage(imgarray[t]);
                ic.DrawImage( img, new Rectangle(0,0,iWidth,iHeight), new Rectangle(x0,y0,iWidth*iXStep,iHeight*iYStep), GraphicsUnit.Pixel  );

                    ChangeColor(imgarray[t], Color.FromArgb(255, 23, 56, 55), Color.FromArgb(0, 128, 0, 128));
                    ChangeColor(imgarray[t], Color.FromArgb(255, 107, 87, 67), Color.FromArgb(255, 255, 255, 255));
                    ChangeColor(imgarray[t], Color.FromArgb(255, 152, 135, 95), Color.FromArgb(255, 0, 0, 0));
                    ChangeColor(imgarray[t], Color.FromArgb(255, 13, 47, 46), Color.FromArgb(0, 128, 0, 128));

                    /*
                ChangeColor(imgarray[t], Color.FromArgb(255,23,56,55), Color.FromArgb(0, 128, 0, 128) );
                ChangeColor(imgarray[t], Color.FromArgb(255,107,87,67), Color.FromArgb(255,0,0,0) );
                ChangeColor(imgarray[t], Color.FromArgb(255,152,135,95), Color.FromArgb(255, 255, 255, 255));
                ChangeColor(imgarray[t], Color.FromArgb(255,13,47,46), Color.FromArgb(0,128,0,128) );
                    */
                    ChangeColor(imgarray[t], Color.FromArgb(255,114,186,129), Color.FromArgb(255,0,0,0) );
                ChangeColor(imgarray[t], Color.FromArgb(255,186,158,91), Color.FromArgb(255,255,255,255) );
                ChangeColor(imgarray[t], Color.FromArgb(255,91,155,105), Color.FromArgb(255,255,255,255) );
                ChangeColor(imgarray[t], Color.FromArgb(255,21,87,85), Color.FromArgb(0,128,0,128) );

                imgarray[t].Save( ".\\Output\\" + sPrefix.Text + n +".png", ImageFormat.Png);

                if (GenerateFlipped.Checked)
                {
                    imgarray[t].RotateFlip(RotateFlipType.RotateNoneFlipX);
                    imgarray[t].Save( ".\\Output\\" + sPrefix.Text + "flipped_" + n+ ".png", ImageFormat.Png);
                }


                ic.Dispose();
                t++;
            }

            MessageBox.Show("Exported " + t + " tiles.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.FileDialog FD = new OpenFileDialog();
            if (FD.ShowDialog() == DialogResult.OK)
            {
                FileName.Text = FD.FileName;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_2(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {

        }

        public int GrabOffsetX = 0;
        public int GrabOffsetY = 0;
        public Bitmap Grab(Image img, int x, int y, int width, int height)
        {
            Bitmap Ret = new Bitmap(width,height);
            Graphics G = Graphics.FromImage(Ret);

            G.DrawImage( img, new Rectangle(0,0,width,height), new Rectangle(x+GrabOffsetX,y+GrabOffsetY,width,height), GraphicsUnit.Pixel );

            G.Dispose();
            return Ret;
        }

        public Bitmap HFlip(Bitmap B)
        {
            Bitmap Ret = new Bitmap(B);
            Ret.RotateFlip(RotateFlipType.Rotate180FlipY);
            return Ret;
        }

        public bool getbit(int n, int bit)
        {
            return (n & (1 << bit)) != 0;
        }

        public bool[] bits(int n)
        {
            bool[] ret = new bool[8];
            for (int x = 0; x < 8; x++)
            {
                ret[x] = getbit(n, x);
            }
            return ret;
        }

        private void ChangeColor(Bitmap s, System.Drawing.Color source, System.Drawing.Color target)
        {
            for (int x = 0; x < s.Width; x++)
            {
                for (int y = 0; y < s.Height; y++)
                {
                    if (s.GetPixel(x, y) == source)
                        s.SetPixel(x, y, target);
                }                
            }
        } 

        private void ChangeAnyColor(Bitmap s, System.Drawing.Color target)
        {
            for (int x = 0; x < s.Width; x++)
            {
                for (int y = 0; y < s.Height; y++)
                {
                    if (s.GetPixel(x, y).A > 0 )
                    {
                        s.SetPixel(x, y, target);
                    }
                }                
            }
        } 

        public void blit(Graphics target, Bitmap source, int x, int y)
        {
            target.DrawImage(source, new Point(x,y));
        }

        private void button3_Click(object sender, EventArgs e)
        {
           Image img = Image.FromFile( FileName.Text );

            int iTopOffset = Convert.ToInt32(sTopOffset.Text);
            int iLeftOffset = Convert.ToInt32(sLeftOffset.Text);

            GrabOffsetX = iLeftOffset;
            GrabOffsetY = iTopOffset;

            int iXStep = Convert.ToInt32(XStep.Text);
            int iYStep = Convert.ToInt32(YStep.Text);
            int iWidth = Convert.ToInt32(sWidth.Text);
            int iHeight = Convert.ToInt32(sHeight.Text);

            int nTilesWide = (img.Width - iLeftOffset)/(iWidth*iXStep);
            int nTilesHigh = (img.Height - iTopOffset)/(iHeight*iYStep);

            Bitmap[] imgarray = new Bitmap[nTilesWide*nTilesHigh];


            Bitmap TopLeft000 = Grab(img, 0, 0, 3, 3);
            Bitmap TopLeft001 = Grab(img, 0, 2, 3, 3);;
            Bitmap TopLeft101 = Grab(img, 64, 96, 3, 3);;
            Bitmap TopLeft100 = Grab(img, 64, 0, 3, 3);;
            Bitmap TopLeft111 = Grab(img, 3, 3, 3, 3);;

            Bitmap TopRight000 = HFlip(TopLeft000);
            Bitmap TopRight001 = HFlip(TopLeft001);
            Bitmap TopRight101 = HFlip(TopLeft101);
            Bitmap TopRight100 = HFlip(TopLeft100);
            Bitmap TopRight111 = Grab(img, 4, 3, 3, 3);;

            Bitmap TopMiddleFull = Grab(img, 3, 24, 10, 3);
            Bitmap TopMiddleEmpty = Grab(img, 3, 0, 10, 3);

            Bitmap LeftFull = Grab(img, 4, 3, 3, 10);
            Bitmap LeftEmpty = Grab(img, 0, 3, 3, 10);

            Bitmap RightFull =  Grab(img, 5, 3, 3, 10);
            Bitmap RightEmpty = HFlip(LeftEmpty);

            Bitmap MiddleFull = Grab(img, 3, 3, 10, 10);
            Bitmap MiddleEmpty = Grab(img, 3, 3, 10, 10);

            Bitmap BotMiddleFull = Grab(img, 3, 13, 10, 11);
            Bitmap BotMiddleEmpty = Grab(img, 3, 37, 10, 11);

            Bitmap BotLeftSideClosedCorner  = Grab(img, 0,37, 3,8);
            Bitmap BotLeftSideClosed  = Grab(img, 16,13, 3,8);
            Bitmap BotLeftSideCorner =  Grab(img, 64,13, 3,8);
            Bitmap BotLeftSideOpen = Grab(img, 0,13, 3,8);
            Bitmap BotLeftSideFull  = Grab(img, 4,3, 3,8);

            Bitmap BotRightSideClosedCorner =  Grab(img, 13,37, 3,8);
            Bitmap BotRightSideClosed =  Grab(img, 24,13, 3,8);
            Bitmap BotRightSideCorner =  Grab(img, 13,13, 3,8);
            Bitmap BotRightSideOpen = Grab(img, 77, 13, 3, 8);
            Bitmap BotRightSideFull  = Grab(img, 5,3, 3,8);;

            Bitmap BotLeftCornerClosed = Grab(img, 0, 45, 3, 3);
            Bitmap BotLeftCornerOpen = Grab(img, 0, 21, 3, 3);
            Bitmap BotLeftCornerFull = Grab(img, 4, 3, 3, 3);

            Bitmap BotRightCornerClosed = HFlip(BotLeftCornerClosed);
            Bitmap BotRightCornerOpen =  Grab(img, 13, 21, 3, 3);
            Bitmap BotRightCornerFull = Grab(img, 3, 3, 3, 3);

            for (int x = 0; x < 256; x++)
            {
                Bitmap ret = new Bitmap(16,24);
                Graphics g = Graphics.FromImage(ret);
                bool[] bit = bits(x);

                // n - 0
                // ne - 1
                // e - 2
                // se - 3
                // s - 4
                // sw - 5
                // w - 6
                // nw - 7

                // top left
                if ( (!bit[6] && !bit[7] && bit[0]) || (!bit[6] && bit[7] && bit[0]))
                {
                    blit(g, TopLeft001, 0,0); 
                }
                else
                if ( (bit[6] && !bit[7] && !bit[0]) || (bit[6] && bit[7] && !bit[0]))
                {
                    blit(g, TopLeft100, 0,0); 
                }
                else
                if (bit[6] && bit[7] && bit[0])
                {
                    blit(g, TopLeft111, 0,0); 
                }
                else if (bit[6] && !bit[7] && bit[0])
                {
                    blit(g, TopLeft101, 0, 0);
                }
                else
                {
                    blit(g, TopLeft000, 0, 0);
                }

                // top right
                if ( (!bit[2] && !bit[1] && bit[0]) || (!bit[2] && bit[1] && bit[0]) )
                {
                    blit(g, TopRight001, 13,0); 
                }
                else
                if ( (bit[2] && !bit[1] && !bit[0]) || (bit[2] && bit[1] && !bit[0]) )
                {
                    blit(g, TopRight100, 13,0); 
                }
                else
                if (bit[2] && bit[1] && bit[0])
                {
                    blit(g, TopRight111, 13,0); 
                }
                else if (bit[2] && !bit[1] && bit[0])
                {
                    blit(g, TopRight101, 13, 0);
                }
                else
                {
                    blit(g, TopRight000, 13, 0);
                }

                // left middle
                if (bit[6])
                {
                    blit(g, LeftFull, 0, 3);
                }
                else
                {
                    blit(g, LeftEmpty, 0, 3);
                }

                // right middle
                if (bit[2])
                {
                    blit(g, RightFull, 13, 3);
                }
                else
                {
                    blit(g, RightEmpty, 13, 3);
                }

                // top middle
                if (bit[0])
                {
                    blit(g, TopMiddleFull, 3, 0);
                }
                else
                {
                    blit(g, TopMiddleEmpty, 3, 0);
                }

                // middle
                blit(g, MiddleFull, 3, 3);
                
                // bot left side
                if (!bit[4])
                {
                    if (!bit[6])
                    {
                        blit(g, BotLeftSideClosedCorner, 0, 13);
                    }
                    else
                    {
                        blit(g, BotLeftSideClosed, 0, 13);
                    }
                }
                else if (bit[6] && bit[5])
                {
                    blit(g, BotLeftSideFull, 0, 13);
                }
                else
                {
                    if (bit[6])
                    {
                        blit(g,BotLeftSideCorner,0,13);
                    }
                    else
                    {
                        blit(g,BotLeftSideOpen,0,13);
                    }
                }

                // bot right side
                if (!bit[4])
                {
                    if (!bit[2])
                    {
                        blit(g,BotRightSideClosedCorner,13,13);
                    }
                    else
                    {
                        blit(g,BotRightSideClosed,13,13);
                    }
                }
                else if (bit[2] && bit[3])
                {
                    blit(g,BotRightSideFull,13,13);
                }
                else
                {
                    if (bit[2])
                    {
                        blit(g,BotRightSideCorner,13,13);
                    }
                    else
                    {
                        blit(g,BotRightSideOpen,13,13);
                    }
                }
                // bot middle
                if (bit[4])
                {
                    blit(g, BotMiddleFull, 3, 13);
                }
                else
                {
                    blit(g, BotMiddleEmpty, 3, 13);
                }

                // bot left corner
                if (!bit[4])
                {
                    blit(g, BotLeftCornerClosed, 0, 21);
                }
                else
                if (bit[4] && bit[5] && bit[6]) 
                {
                    blit(g, BotLeftCornerFull, 0,21);
                }
                else
                {
                    blit(g, BotLeftCornerOpen, 0, 21);
                }
                
                // bot right corner
                 if (!bit[4])
                {
                    blit(g, BotRightCornerClosed, 13, 21);
                }
                else
                if (bit[4] && bit[3] && bit[2]) 
                {
                    blit(g, BotRightCornerFull, 13,21);
                }
                else
                {
                    blit(g, BotRightCornerOpen, 13, 21);
                }

                g.Dispose();

                ChangeColor(ret, Color.FromArgb(255,23,56,55), Color.FromArgb(255,255,255,255) );
                ChangeColor(ret, Color.FromArgb(255,107,87,67), Color.FromArgb(255,0,0,0) );
                ChangeColor(ret, Color.FromArgb(255,152,135,95), Color.FromArgb(0,0,0,0) );

                ret.Save(".\\Output\\" + sPrefix.Text + "-"+
                (bit[0]?"1":"0")+
                (bit[1]?"1":"0")+
                (bit[2]?"1":"0")+
                (bit[3]?"1":"0")+
                (bit[4]?"1":"0")+
                (bit[5]?"1":"0")+
                (bit[6]?"1":"0")+
                (bit[7]?"1":"0")+
                ".bmp");
            }
            
            MessageBox.Show("Exported wall.");
        }

        private void GenWaterClick(object sender, EventArgs e)
        {
           Image img = Image.FromFile( FileName.Text );

            int iTopOffset = Convert.ToInt32(sTopOffset.Text);
            int iLeftOffset = Convert.ToInt32(sLeftOffset.Text);

            GrabOffsetX = iLeftOffset;
            GrabOffsetY = iTopOffset;

            int iXStep = Convert.ToInt32(XStep.Text);
            int iYStep = Convert.ToInt32(YStep.Text);
            int iWidth = Convert.ToInt32(sWidth.Text);
            int iHeight = Convert.ToInt32(sHeight.Text);

            int nTilesWide = (img.Width - iLeftOffset)/(iWidth*iXStep);
            int nTilesHigh = (img.Height - iTopOffset)/(iHeight*iYStep);

            Bitmap[] imgarray = new Bitmap[nTilesWide*nTilesHigh];

            
            Bitmap FillTile = Grab(img, 87, 67, 16, 24);

            for (int x = 0; x < 256; x++)
            {
                Bitmap ret = new Bitmap(16,24);
                Graphics g = Graphics.FromImage(ret);
                bool[] bit = bits(x);

                // n - 0
                // ne - 1
                // e - 2
                // se - 3
                // s - 4
                // sw - 5
                // w - 6
                // nw - 7

                // top left
                /*
                blit(g, FillTile, 0, 0);

                if (!bit[6]) // No water west
                {
                    blit(g, LeftEdge, 0, 0);
                }

                if (!bit[2]) // No water east
                {
                    blit(g, RightEdge, 12,0);
                }

                if (!bit[0]) // No water north
                {
                    if (bit[2] && bit[6]) // Water east and west
                    {
                        blit(g, TopEdgeWaterEastAndWest, 0, 0);
                    }
                    else if (bit[2])
                    {
                        blit(g, TopEdgeWaterToEast, 0, 0);
                    }
                    else if (bit[6])
                    {
                        blit(g, TopEdgeWaterToWest, 0, 0);
                    }
                    else
                    {
                        blit(g, TopEdgeWaterNeither, 0, 0);
                    }
                }

                if (!bit[4]) // No water south
                {
                    if (bit[2] && bit[6]) // Water east and west
                    {
                        blit(g, BottomEdgeWaterEastAndWest, 16, 15);
                    }
                    else if (bit[2])
                    {
                        blit(g, BottomEdgeWaterToEast, 0, 14);
                    }
                    else if (bit[6])
                    {
                        blit(g, BottomEdgeWaterToWest, 0, 14);
                    }
                    else
                    {
                        blit(g, BottomEdgeWaterNeither, 0, 16);
                    }
                }

                g.Dispose();

                ChangeColor(ret, Color.FromArgb(255,23,56,55), Color.FromArgb(255,255,255,255) );
                ChangeColor(ret, Color.FromArgb(255,107,87,67), Color.FromArgb(0,0,0,0));
                ChangeColor(ret, Color.FromArgb(255,152,135,95),  Color.FromArgb(255,0,0,0) );

                ret.Save(".\\Output\\" + sPrefix.Text + "-"+
                (bit[0]?"1":"0")+
                (bit[1]?"1":"0")+
                (bit[2]?"1":"0")+
                (bit[3]?"1":"0")+
                (bit[4]?"1":"0")+
                (bit[5]?"1":"0")+
                (bit[6]?"1":"0")+
                (bit[7]?"1":"0")+
                ".bmp");*/
            }
            
            MessageBox.Show("Exported water.");
        }

        private static readonly StringBuilder Builder = new StringBuilder();
        private char CheckPixel(Bitmap Template, int X, int Y) => Template.GetPixel(X, Y).R < 128 ? '1' : '0';
        private string TemplateToConnection(Bitmap Template, int X, int Y)
        {
	        X *= 3;
	        Y *= 3;
	        if (CheckPixel(Template, X + 1, Y + 1) == '0') return null;

	        Builder.Clear();
	        Builder.Append(CheckPixel(Template, X + 1, Y + 0)); // N
	        Builder.Append(CheckPixel(Template, X + 2, Y + 0)); // NE
	        Builder.Append(CheckPixel(Template, X + 2, Y + 1)); // E
	        Builder.Append(CheckPixel(Template, X + 2, Y + 2)); // SE
	        Builder.Append(CheckPixel(Template, X + 1, Y + 2)); // S
	        Builder.Append(CheckPixel(Template, X + 0, Y + 2)); // SW
	        Builder.Append(CheckPixel(Template, X + 0, Y + 1)); // W
	        Builder.Append(CheckPixel(Template, X + 0, Y + 0)); // NW
	        return Builder.ToString();
        }

        private void SliceTemplateClick(object sender, EventArgs e)
        {
	        var templatePath = "";
	        var FD = new OpenFileDialog();
	        if (FD.ShowDialog() == DialogResult.OK)
	        {
		        templatePath = FD.FileName;
	        }

	        if (string.IsNullOrEmpty(templatePath)) return;
	        var img = Image.FromFile( FileName.Text );
	        var template = new Bitmap(templatePath);
	        if (template.Height % 3 != 0)
	        {
		        MessageBox.Show("Template height is not evenly divisible by 3.");
		        return;
	        }
	        if (template.Width % 3 != 0)
	        {
		        MessageBox.Show("Template width is not evenly divisible by 3.");
		        return;
	        }

            var top = GrabOffsetX = Convert.ToInt32(sTopOffset.Text);
            var left = GrabOffsetY = Convert.ToInt32(sLeftOffset.Text);
            var xStep = Convert.ToInt32(XStep.Text);
            var yStep = Convert.ToInt32(YStep.Text);
            var tileWidth = Convert.ToInt32(sWidth.Text);
            var tileHeight = Convert.ToInt32(sHeight.Text);

            var horizontal = (img.Width - left)/(tileWidth*xStep);
            var vertical = (img.Height - top)/(tileHeight*yStep);
            if (horizontal != template.Width / 3)
            {
	            MessageBox.Show($"Mismatched horizontal tiles, image: {horizontal}, template: {template.Width / 3}.");
	            return;
            }
            if (vertical != template.Height / 3)
            {
	            MessageBox.Show($"Mismatched vertical tiles, image: {vertical}, template: {template.Height / 3}.");
	            return;
            }

            var t = 0;
            var set = new Dictionary<string, Point>();
            for (int x = 0; x < horizontal; x++)
            for (int y = 0; y < vertical; y++)
            {
	            var suffix = TemplateToConnection(template, x, y);
	            if (string.IsNullOrEmpty(suffix)) continue;
	            if (set.TryGetValue(suffix, out var last))
	            {
		            MessageBox.Show($"Tile [{x}, {y}] is identical to [{last.X}, {last.Y}].");
		            continue;
	            }
	            
	            var sliced = Grab(img, xStep * x * tileWidth, yStep * y * tileHeight, tileWidth, tileHeight);
	            sliced.Save(
		            ".\\Output\\"
		            + sPrefix.Text + "-"
		            + suffix
		            + ".png",
		            ImageFormat.Png
	            );

	            t++;
	            set[suffix] = new Point(x, y);
            }

            Builder.Clear();
            if (GenerateFull.Checked)
            {
	            for (int i = 0; i <= byte.MaxValue; i++)
	            {
		            var suffix = Convert.ToString(i, 2).PadLeft(8, '0');
		            if (!set.TryGetValue(suffix, out var point))
		            {
			            var stripped = i & 0xAA;
			            if (stripped != i)
			            {
				            var sub = Convert.ToString(stripped, 2).PadLeft(8, '0');
				            if (set.TryGetValue(sub, out point))
				            {
					            var sliced = Grab(img, xStep * point.X * tileWidth, yStep * point.Y * tileHeight, tileWidth, tileHeight);
					            sliced.Save(
						            ".\\Output\\"
						            + sPrefix.Text + "-"
						            + suffix
						            + ".png",
						            ImageFormat.Png
					            );

					            t++;
					            continue;
				            }
			            }

			            if (Builder.Length != 0) Builder.Append(", ");
			            Builder.Append(suffix);
		            }
	            }
            }

            if (Builder.Length != 0) Builder.Insert(0, " Missing ");
            Builder.Insert(0, $"Exported {t} tiles.");

            MessageBox.Show(Builder.ToString());
        }

        private void wallmaker_C_Clicked(object sender, EventArgs e)
        {
            Image img = Image.FromFile(FileName.Text);

            int iTopOffset = Convert.ToInt32(sTopOffset.Text);
            int iLeftOffset = Convert.ToInt32(sLeftOffset.Text);

            GrabOffsetX = iLeftOffset;
            GrabOffsetY = iTopOffset;

            int iXStep = Convert.ToInt32(XStep.Text);
            int iYStep = Convert.ToInt32(YStep.Text);
            int iWidth = Convert.ToInt32(sWidth.Text);
            int iHeight = Convert.ToInt32(sHeight.Text);

            int nTilesWide = (img.Width - iLeftOffset) / (iWidth * iXStep);
            int nTilesHigh = (img.Height - iTopOffset) / (iHeight * iYStep);

            Bitmap[] imgarray = new Bitmap[nTilesWide * nTilesHigh];

            Bitmap TopLeft000 = Grab(img, 0, 0, 3, 3);
            Bitmap TopLeft001 = Grab(img, 0, 2, 3, 3); ;
            Bitmap TopLeft101 = Grab(img, 64, 96, 3, 3); ;
            Bitmap TopLeft100 = Grab(img, 64, 0, 3, 3); ;
            Bitmap TopLeft111 = Grab(img, 3, 3, 3, 3); ;

            Bitmap TopRight000 = HFlip(TopLeft000);
            Bitmap TopRight001 = HFlip(TopLeft001);
            Bitmap TopRight101 = HFlip(TopLeft101);
            Bitmap TopRight100 = HFlip(TopLeft100);
            Bitmap TopRight111 = Grab(img, 4, 3, 3, 3); ;

            Bitmap TopMiddleFull = Grab(img, 3, 24, 10, 3);
            Bitmap TopMiddleEmpty = Grab(img, 3, 0, 10, 3);

            Bitmap LeftFull = Grab(img, 4, 3, 3, 10);
            Bitmap LeftEmpty = Grab(img, 0, 3, 3, 10);

            Bitmap RightFull = Grab(img, 5, 3, 3, 10);
            Bitmap RightEmpty = HFlip(LeftEmpty);

            Bitmap MiddleFull = Grab(img, 3, 3, 10, 10);
            Bitmap MiddleEmpty = Grab(img, 3, 3, 10, 10);

            Bitmap BotMiddleFull = Grab(img, 3, 13, 10, 11);
            Bitmap BotMiddleEmpty = Grab(img, 3, 37, 10, 11);

            Bitmap BotLeftSideClosedCorner = Grab(img, 0, 37, 3, 8);
            Bitmap BotLeftSideClosed = Grab(img, 16, 13, 3, 8);
            Bitmap BotLeftSideCorner = Grab(img, 64, 13, 3, 8);
            Bitmap BotLeftSideOpen = Grab(img, 0, 13, 3, 8);
            Bitmap BotLeftSideFull = Grab(img, 4, 3, 3, 8);

            Bitmap BotRightSideClosedCorner = Grab(img, 13, 37, 3, 8);
            Bitmap BotRightSideClosed = Grab(img, 29, 13, 3, 8);
            Bitmap BotRightSideCorner = Grab(img, 13, 13, 3, 8);
            Bitmap BotRightSideOpen = Grab(img, 77, 13, 3, 8);
            Bitmap BotRightSideFull = Grab(img, 5, 3, 3, 8); ;

            Bitmap BotLeftCornerClosed = Grab(img, 0, 45, 3, 3);
            Bitmap BotLeftCornerOpen = Grab(img, 16, 21, 3, 3);
            Bitmap BotLeftCornerWall = Grab(img, 0, 21, 3, 3);
            Bitmap BotLeftCornerFull = Grab(img, 4, 3, 3, 3);

            Bitmap BotRightCornerClosed = Grab(img, 13,45,3,3);
            Bitmap BotRightCornerOpen = Grab(img, 29, 21, 3, 3);
            Bitmap BotRightCornerWall = Grab(img, 13, 21, 3, 3);
            Bitmap BotRightCornerFull = Grab(img, 3, 3, 3, 3);

            for (int x = 0; x < 256; x++)
            {
                Bitmap ret = new Bitmap(16, 24);
                Graphics g = Graphics.FromImage(ret);
                bool[] bit = bits(x);

                // n - 0
                // ne - 1
                // e - 2
                // se - 3
                // s - 4
                // sw - 5
                // w - 6
                // nw - 7

                // top left
                if ((!bit[6] && !bit[7] && bit[0]) || (!bit[6] && bit[7] && bit[0]))
                {
                    blit(g, TopLeft001, 0, 0);
                }
                else
                if ((bit[6] && !bit[7] && !bit[0]) || (bit[6] && bit[7] && !bit[0]))
                {
                    blit(g, TopLeft100, 0, 0);
                }
                else
                if (bit[6] && bit[7] && bit[0])
                {
                    blit(g, TopLeft111, 0, 0);
                }
                else if (bit[6] && !bit[7] && bit[0])
                {
                    blit(g, TopLeft101, 0, 0);
                }
                else
                {
                    blit(g, TopLeft000, 0, 0);
                }

                // top right
                if ((!bit[2] && !bit[1] && bit[0]) || (!bit[2] && bit[1] && bit[0]))
                {
                    blit(g, TopRight001, 13, 0);
                }
                else
                if ((bit[2] && !bit[1] && !bit[0]) || (bit[2] && bit[1] && !bit[0]))
                {
                    blit(g, TopRight100, 13, 0);
                }
                else
                if (bit[2] && bit[1] && bit[0])
                {
                    blit(g, TopRight111, 13, 0);
                }
                else if (bit[2] && !bit[1] && bit[0])
                {
                    blit(g, TopRight101, 13, 0);
                }
                else
                {
                    blit(g, TopRight000, 13, 0);
                }

                // left middle
                if (bit[6])
                {
                    blit(g, LeftFull, 0, 3);
                }
                else
                {
                    blit(g, LeftEmpty, 0, 3);
                }

                // right middle
                if (bit[2])
                {
                    blit(g, RightFull, 13, 3);
                }
                else
                {
                    blit(g, RightEmpty, 13, 3);
                }

                // top middle
                if (bit[0])
                {
                    blit(g, TopMiddleFull, 3, 0);
                }
                else
                {
                    blit(g, TopMiddleEmpty, 3, 0);
                }

                // middle
                blit(g, MiddleFull, 3, 3);

                // bot left side
                if (!bit[4])
                {
                    if (!bit[6])
                    {
                        blit(g, BotLeftSideClosedCorner, 0, 13);
                    }
                    else
                    {
                        blit(g, BotLeftSideClosed, 0, 13);
                    }
                }
                else if (bit[6] && bit[5])
                {
                    blit(g, BotLeftSideFull, 0, 13);
                }
                else
                {
                    if (bit[6])
                    {
                        blit(g, BotLeftSideCorner, 0, 13);
                    }
                    else
                    {
                        blit(g, BotLeftSideOpen, 0, 13);
                    }
                }

                // bot right side
                if (!bit[4])
                {
                    if (!bit[2])
                    {
                        blit(g, BotRightSideClosedCorner, 13, 13);
                    }
                    else
                    {
                        blit(g, BotRightSideClosed, 13, 13);
                    }
                }
                else if (bit[2] && bit[3])
                {
                    blit(g, BotRightSideFull, 13, 13);
                }
                else
                {
                    if (bit[2])
                    {
                        blit(g, BotRightSideCorner, 13, 13);
                    }
                    else
                    {
                        blit(g, BotRightSideOpen, 13, 13);
                    }
                }
                // bot middle
                if (bit[4])
                {
                    blit(g, BotMiddleFull, 3, 13);
                }
                else
                {
                    blit(g, BotMiddleEmpty, 3, 13);
                }

                // bot left corner
                if (!bit[4])
                {
                    if( bit[6])
                    {
                        blit(g, BotLeftCornerOpen, 0, 21);
                    }
                    else
                    {
                        blit(g, BotLeftCornerClosed, 0, 21);
                    }
                }
                else
                if (bit[4] && bit[5] && bit[6])
                {
                    blit(g, BotLeftCornerFull, 0, 21);
                }
                else
                {
                    blit(g, BotLeftCornerWall, 0, 21);
                }

                // bot right corner
                if (!bit[4])
                {
                    if( bit[2] )
                    {
                        blit(g, BotRightCornerOpen, 13, 21);
                    }
                    else
                    {
                        blit(g, BotRightCornerClosed, 13, 21);
                    }
                }
                else
                if (bit[4] && bit[3] && bit[2])
                {
                    blit(g, BotRightCornerFull, 13, 21);
                }
                else
                {
                    blit(g, BotRightCornerWall, 13, 21);
                }

                g.Dispose();

                ChangeColor(ret, Color.FromArgb(255, 23, 56, 55), Color.FromArgb(255, 255, 255, 255));
                ChangeColor(ret, Color.FromArgb(255, 107, 87, 67), Color.FromArgb(255, 0, 0, 0));
                ChangeColor(ret, Color.FromArgb(255, 152, 135, 95), Color.FromArgb(0, 0, 0, 0));

                ret.Save(".\\Output\\" + sPrefix.Text + "-" +
                (bit[0] ? "1" : "0") +
                (bit[1] ? "1" : "0") +
                (bit[2] ? "1" : "0") +
                (bit[3] ? "1" : "0") +
                (bit[4] ? "1" : "0") +
                (bit[5] ? "1" : "0") +
                (bit[6] ? "1" : "0") +
                (bit[7] ? "1" : "0") +
                ".png", System.Drawing.Imaging.ImageFormat.Png);
            }

            MessageBox.Show("Exported wall.");
        }

        private void fencemaker_click( object sender, EventArgs e )
        {
            Image img = Image.FromFile(FileName.Text);

            int iTopOffset = Convert.ToInt32(sTopOffset.Text);
            int iLeftOffset = Convert.ToInt32(sLeftOffset.Text);

            GrabOffsetX = iLeftOffset;
            GrabOffsetY = iTopOffset;

            Bitmap _SE = Grab(img, 0, 0, 16, 24);
            Bitmap _SEW = Grab(img, 16, 0, 16, 24);
            Bitmap _SW = Grab(img, 32, 0, 16, 24);
            Bitmap _S = Grab(img, 48, 0, 16, 24);

            Bitmap _NSE = Grab(img, 0, 24, 16, 24);
            Bitmap _NSEW = Grab(img, 16, 24, 16, 24);
            Bitmap _NSW = Grab(img, 32, 24, 16, 24);
            Bitmap _NS = Grab(img, 48, 24, 16, 24);

            Bitmap _NE = Grab(img, 0, 48, 16, 24);
            Bitmap _NEW = Grab(img, 16, 48, 16, 24);
            Bitmap _NW = Grab(img, 32, 48, 16, 24);
            Bitmap _N = Grab(img, 48, 48, 16, 24);

            Bitmap _E = Grab(img, 0, 72, 16, 24);
            Bitmap _EW = Grab(img, 16, 72, 16, 24);
            Bitmap _W = Grab(img, 32, 72, 16, 24);
            Bitmap _ = Grab(img, 48, 72, 16, 24);

            export( _SE, "se");
            export( _SEW, "sew");
            export( _SW, "sw");
            export( _S, "s");
            export( _NSE, "nse");
            export( _NSEW, "nsew");
            export( _NSW, "nsw");
            export( _NS, "ns");
            export( _NE, "ne");
            export( _NEW, "new");
            export( _NW, "nw");
            export( _N, "n");
            export( _E, "e");
            export( _EW, "ew");
            export( _W, "w");
            export( _, "");
        }

        void export( Bitmap bmp, string fileName )
        {
            ChangeColor(bmp, Color.FromArgb(255, 23, 56, 55), Color.FromArgb(0, 0, 0, 0));
            ChangeColor(bmp, Color.FromArgb(255, 66, 163, 184), Color.FromArgb(255, 0, 0, 0));
            ChangeColor(bmp, Color.FromArgb(255, 107, 87, 67), Color.FromArgb(255, 0, 0, 0));
            ChangeColor(bmp, Color.FromArgb(255, 152, 135, 95), Color.FromArgb(255, 255, 255, 255));

            bmp.Save(".\\Output\\" + sPrefix.Text + fileName 
                + ".png", System.Drawing.Imaging.ImageFormat.Png);
        }

        private void sliceSingleColorSprite( object sender, EventArgs e )
        {
            Image img = Image.FromFile( FileName.Text );

            int iTopOffset = Convert.ToInt32(sTopOffset.Text);
            int iLeftOffset = Convert.ToInt32(sLeftOffset.Text);
            int iXStep = Convert.ToInt32(XStep.Text);
            int iYStep = Convert.ToInt32(YStep.Text);
            int iWidth = Convert.ToInt32(sWidth.Text);
            int iHeight = Convert.ToInt32(sHeight.Text);

            int nTilesWide = (img.Width - iLeftOffset)/(iWidth*iXStep);
            int nTilesHigh = (img.Height - iTopOffset)/(iHeight*iYStep);

            Bitmap[] imgarray = new Bitmap[nTilesWide*nTilesHigh];

            int t = 0;
            int n = 0;
            for( int y=0;y<nTilesHigh;y++)
            for( int x=0;x<nTilesWide;x++ )            
            {
                n++;
                int x0 = iLeftOffset+(x*(iWidth*iXStep));
                int y0 = iTopOffset+(y*(iHeight*iYStep));
                int x1 = x0+(iWidth*iXStep);
                int y1 = y0+(iHeight*iYStep);

                PointF S = new PointF(x0, y0);
                PointF E = new PointF(x1,y1);

                imgarray[t] = new Bitmap(iWidth,iHeight);
                Graphics ic = Graphics.FromImage(imgarray[t]);
                ic.DrawImage( img, new Rectangle(0,0,iWidth,iHeight), new Rectangle(x0,y0,iWidth*iXStep,iHeight*iYStep), GraphicsUnit.Pixel  );

                ChangeAnyColor( imgarray[t], Color.FromArgb(255,255,255,255));

                imgarray[t].Save( ".\\Output\\" + sPrefix.Text + n +".png", System.Drawing.Imaging.ImageFormat.Png );

                if (GenerateFlipped.Checked)
                {
                    imgarray[t].RotateFlip(RotateFlipType.RotateNoneFlipX);
                    imgarray[t].Save( ".\\Output\\" + sPrefix.Text + "flipped_" + n+".png", System.Drawing.Imaging.ImageFormat.Png );
                }


                ic.Dispose();
                t++;
            }
        }

		private void button8_Click(object sender, EventArgs e)
		{
			Image img = Image.FromFile(FileName.Text);

			int iTopOffset = Convert.ToInt32(sTopOffset.Text);
			int iLeftOffset = Convert.ToInt32(sLeftOffset.Text);
			int iXStep = Convert.ToInt32(XStep.Text);
			int iYStep = Convert.ToInt32(YStep.Text);
			int iWidth = Convert.ToInt32(sWidth.Text);
			int iHeight = Convert.ToInt32(sHeight.Text);

			int nTilesWide = (img.Width - iLeftOffset) / (iWidth * iXStep);
			int nTilesHigh = (img.Height - iTopOffset) / (iHeight * iYStep);

			Bitmap[] imgarray = new Bitmap[nTilesWide * nTilesHigh];

			int t = 0;
			int n = 0;
			for (int y = 0; y < nTilesHigh; y++)
			{
				for (int x = 0; x < nTilesWide; x++)
				{
					n++;
					int x0 = iLeftOffset + (x * (iWidth * iXStep));
					int y0 = iTopOffset + (y * (iHeight * iYStep));
					int x1 = x0 + (iWidth * iXStep);
					int y1 = y0 + (iHeight * iYStep);

					PointF S = new PointF(x0, y0);
					PointF E = new PointF(x1, y1);

					imgarray[t] = new Bitmap(iWidth, iHeight);
					Graphics ic = Graphics.FromImage(imgarray[t]);
					ic.DrawImage(img, new Rectangle(0, 0, iWidth, iHeight), new Rectangle(x0, y0, iWidth * iXStep, iHeight * iYStep), GraphicsUnit.Pixel);

					ChangeColor(imgarray[t], Color.FromArgb(255, 23, 56, 55), Color.FromArgb(0, 128, 0, 128));
					ChangeColor(imgarray[t], Color.FromArgb(255, 107, 87, 67), Color.FromArgb(255, 255, 255, 255));
					ChangeColor(imgarray[t], Color.FromArgb(255, 152, 135, 95), Color.FromArgb(255, 0, 0, 0));
					ChangeColor(imgarray[t], Color.FromArgb(255, 13, 47, 46), Color.FromArgb(255, 255, 255, 255));

					ChangeColor(imgarray[t], Color.FromArgb(255, 114, 186, 129), Color.FromArgb(255, 0, 0, 0));
					ChangeColor(imgarray[t], Color.FromArgb(255, 186, 158, 91), Color.FromArgb(0, 128, 0, 128));
					ChangeColor(imgarray[t], Color.FromArgb(255, 91, 155, 105), Color.FromArgb(255, 255, 255, 255));
					ChangeColor(imgarray[t], Color.FromArgb(255, 21, 87, 85), Color.FromArgb(0, 128, 0, 128));

					imgarray[t].Save(".\\Output\\" + sPrefix.Text + n + ".png", ImageFormat.Png);

					if (GenerateFlipped.Checked)
					{
						imgarray[t].RotateFlip(RotateFlipType.RotateNoneFlipX);
						imgarray[t].Save(".\\Output\\" + sPrefix.Text + "flipped_" + n + ".png", ImageFormat.Png);
					}


					ic.Dispose();
					t++;
				}

				MessageBox.Show("Exported " + t + " tiles.");
			}
		}

		private void OpenOutputClick(object sender, EventArgs e)
		{
			Process.Start(".\\Output\\");
		}
    }

}

﻿namespace ImageSlicer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
	        this.FileName = new System.Windows.Forms.TextBox();
	        this.button1 = new System.Windows.Forms.Button();
	        this.button2 = new System.Windows.Forms.Button();
	        this.sWidth = new System.Windows.Forms.TextBox();
	        this.label1 = new System.Windows.Forms.Label();
	        this.label2 = new System.Windows.Forms.Label();
	        this.sHeight = new System.Windows.Forms.TextBox();
	        this.sTopOffset = new System.Windows.Forms.TextBox();
	        this.label4 = new System.Windows.Forms.Label();
	        this.sLeftOffset = new System.Windows.Forms.TextBox();
	        this.label3 = new System.Windows.Forms.Label();
	        this.sPrefix = new System.Windows.Forms.TextBox();
	        this.label5 = new System.Windows.Forms.Label();
	        this.label6 = new System.Windows.Forms.Label();
	        this.YStep = new System.Windows.Forms.TextBox();
	        this.XStep = new System.Windows.Forms.TextBox();
	        this.button3 = new System.Windows.Forms.Button();
	        this.button4 = new System.Windows.Forms.Button();
	        this.GenerateFlipped = new System.Windows.Forms.CheckBox();
	        this.button5 = new System.Windows.Forms.Button();
	        this.button6 = new System.Windows.Forms.Button();
	        this.button7 = new System.Windows.Forms.Button();
	        this.button8 = new System.Windows.Forms.Button();
	        this.button9 = new System.Windows.Forms.Button();
	        this.GenerateFull = new System.Windows.Forms.CheckBox();
	        this.button10 = new System.Windows.Forms.Button();
	        this.SuspendLayout();
	        // 
	        // FileName
	        // 
	        this.FileName.Location = new System.Drawing.Point(22, 13);
	        this.FileName.Name = "FileName";
	        this.FileName.Size = new System.Drawing.Size(394, 20);
	        this.FileName.TabIndex = 0;
	        this.FileName.Text = "Input\\walls_bone_cropped.png";
	        this.FileName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
	        // 
	        // button1
	        // 
	        this.button1.Location = new System.Drawing.Point(22, 156);
	        this.button1.Name = "button1";
	        this.button1.Size = new System.Drawing.Size(172, 23);
	        this.button1.TabIndex = 1;
	        this.button1.Text = "Slice";
	        this.button1.UseVisualStyleBackColor = true;
	        this.button1.Click += new System.EventHandler(this.button1_Click);
	        // 
	        // button2
	        // 
	        this.button2.Location = new System.Drawing.Point(422, 13);
	        this.button2.Name = "button2";
	        this.button2.Size = new System.Drawing.Size(102, 23);
	        this.button2.TabIndex = 2;
	        this.button2.Text = "Browse...";
	        this.button2.UseVisualStyleBackColor = true;
	        this.button2.Click += new System.EventHandler(this.button2_Click);
	        // 
	        // sWidth
	        // 
	        this.sWidth.Location = new System.Drawing.Point(22, 59);
	        this.sWidth.Name = "sWidth";
	        this.sWidth.Size = new System.Drawing.Size(81, 20);
	        this.sWidth.TabIndex = 3;
	        this.sWidth.Text = "16";
	        this.sWidth.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
	        // 
	        // label1
	        // 
	        this.label1.AutoSize = true;
	        this.label1.Location = new System.Drawing.Point(22, 40);
	        this.label1.Name = "label1";
	        this.label1.Size = new System.Drawing.Size(35, 13);
	        this.label1.TabIndex = 4;
	        this.label1.Text = "Width";
	        // 
	        // label2
	        // 
	        this.label2.AutoSize = true;
	        this.label2.Location = new System.Drawing.Point(113, 40);
	        this.label2.Name = "label2";
	        this.label2.Size = new System.Drawing.Size(38, 13);
	        this.label2.TabIndex = 6;
	        this.label2.Text = "Height";
	        this.label2.Click += new System.EventHandler(this.label2_Click);
	        // 
	        // sHeight
	        // 
	        this.sHeight.Location = new System.Drawing.Point(113, 59);
	        this.sHeight.Name = "sHeight";
	        this.sHeight.Size = new System.Drawing.Size(81, 20);
	        this.sHeight.TabIndex = 5;
	        this.sHeight.Text = "24";
	        this.sHeight.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
	        // 
	        // sTopOffset
	        // 
	        this.sTopOffset.Location = new System.Drawing.Point(231, 59);
	        this.sTopOffset.Name = "sTopOffset";
	        this.sTopOffset.Size = new System.Drawing.Size(81, 20);
	        this.sTopOffset.TabIndex = 7;
	        this.sTopOffset.Text = "0";
	        // 
	        // label4
	        // 
	        this.label4.AutoSize = true;
	        this.label4.Location = new System.Drawing.Point(318, 40);
	        this.label4.Name = "label4";
	        this.label4.Size = new System.Drawing.Size(53, 13);
	        this.label4.TabIndex = 10;
	        this.label4.Text = "LeftOffset";
	        // 
	        // sLeftOffset
	        // 
	        this.sLeftOffset.Location = new System.Drawing.Point(318, 59);
	        this.sLeftOffset.Name = "sLeftOffset";
	        this.sLeftOffset.Size = new System.Drawing.Size(81, 20);
	        this.sLeftOffset.TabIndex = 9;
	        this.sLeftOffset.Text = "0";
	        // 
	        // label3
	        // 
	        this.label3.AutoSize = true;
	        this.label3.Location = new System.Drawing.Point(228, 40);
	        this.label3.Name = "label3";
	        this.label3.Size = new System.Drawing.Size(54, 13);
	        this.label3.TabIndex = 11;
	        this.label3.Text = "TopOffset";
	        this.label3.Click += new System.EventHandler(this.label3_Click);
	        // 
	        // sPrefix
	        // 
	        this.sPrefix.Location = new System.Drawing.Point(22, 102);
	        this.sPrefix.Name = "sPrefix";
	        this.sPrefix.Size = new System.Drawing.Size(397, 20);
	        this.sPrefix.TabIndex = 12;
	        this.sPrefix.Text = "tile_";
	        this.sPrefix.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
	        // 
	        // label5
	        // 
	        this.label5.AutoSize = true;
	        this.label5.Location = new System.Drawing.Point(419, 40);
	        this.label5.Name = "label5";
	        this.label5.Size = new System.Drawing.Size(36, 13);
	        this.label5.TabIndex = 16;
	        this.label5.Text = "XStep";
	        this.label5.Click += new System.EventHandler(this.label5_Click);
	        // 
	        // label6
	        // 
	        this.label6.AutoSize = true;
	        this.label6.Location = new System.Drawing.Point(509, 40);
	        this.label6.Name = "label6";
	        this.label6.Size = new System.Drawing.Size(36, 13);
	        this.label6.TabIndex = 15;
	        this.label6.Text = "YStep";
	        this.label6.Click += new System.EventHandler(this.label6_Click);
	        // 
	        // YStep
	        // 
	        this.YStep.Location = new System.Drawing.Point(509, 59);
	        this.YStep.Name = "YStep";
	        this.YStep.Size = new System.Drawing.Size(81, 20);
	        this.YStep.TabIndex = 14;
	        this.YStep.Text = "1";
	        this.YStep.TextChanged += new System.EventHandler(this.textBox1_TextChanged_2);
	        // 
	        // XStep
	        // 
	        this.XStep.Location = new System.Drawing.Point(422, 59);
	        this.XStep.Name = "XStep";
	        this.XStep.Size = new System.Drawing.Size(81, 20);
	        this.XStep.TabIndex = 13;
	        this.XStep.Text = "1";
	        this.XStep.TextChanged += new System.EventHandler(this.textBox2_TextChanged_1);
	        // 
	        // button3
	        // 
	        this.button3.Location = new System.Drawing.Point(22, 243);
	        this.button3.Name = "button3";
	        this.button3.Size = new System.Drawing.Size(172, 23);
	        this.button3.TabIndex = 17;
	        this.button3.Text = "Wallmaker (legacy)";
	        this.button3.UseVisualStyleBackColor = true;
	        this.button3.Click += new System.EventHandler(this.button3_Click);
	        // 
	        // button4
	        // 
	        this.button4.Location = new System.Drawing.Point(22, 214);
	        this.button4.Name = "button4";
	        this.button4.Size = new System.Drawing.Size(172, 23);
	        this.button4.TabIndex = 18;
	        this.button4.Text = "Watermaker";
	        this.button4.UseVisualStyleBackColor = true;
	        this.button4.Click += new System.EventHandler(this.GenWaterClick);
	        // 
	        // GenerateFlipped
	        // 
	        this.GenerateFlipped.AutoSize = true;
	        this.GenerateFlipped.Location = new System.Drawing.Point(444, 102);
	        this.GenerateFlipped.Name = "GenerateFlipped";
	        this.GenerateFlipped.Size = new System.Drawing.Size(130, 17);
	        this.GenerateFlipped.TabIndex = 19;
	        this.GenerateFlipped.Text = "Also Generate Flipped";
	        this.GenerateFlipped.UseVisualStyleBackColor = true;
	        // 
	        // button5
	        // 
	        this.button5.Location = new System.Drawing.Point(22, 272);
	        this.button5.Name = "button5";
	        this.button5.Size = new System.Drawing.Size(172, 23);
	        this.button5.TabIndex = 20;
	        this.button5.Text = "Wallmaker (C layout)";
	        this.button5.UseVisualStyleBackColor = true;
	        this.button5.Click += new System.EventHandler(this.wallmaker_C_Clicked);
	        // 
	        // button6
	        // 
	        this.button6.Location = new System.Drawing.Point(22, 301);
	        this.button6.Name = "button6";
	        this.button6.Size = new System.Drawing.Size(172, 23);
	        this.button6.TabIndex = 21;
	        this.button6.Text = "Fencemaker";
	        this.button6.UseVisualStyleBackColor = true;
	        this.button6.Click += new System.EventHandler(this.fencemaker_click);
	        // 
	        // button7
	        // 
	        this.button7.Location = new System.Drawing.Point(200, 156);
	        this.button7.Name = "button7";
	        this.button7.Size = new System.Drawing.Size(172, 23);
	        this.button7.TabIndex = 22;
	        this.button7.Text = "Slice Monocolor Sprite";
	        this.button7.UseVisualStyleBackColor = true;
	        this.button7.Click += new System.EventHandler(this.sliceSingleColorSprite);
	        // 
	        // button8
	        // 
	        this.button8.Location = new System.Drawing.Point(378, 156);
	        this.button8.Name = "button8";
	        this.button8.Size = new System.Drawing.Size(172, 23);
	        this.button8.TabIndex = 23;
	        this.button8.Text = "Slice Inverse";
	        this.button8.UseVisualStyleBackColor = true;
	        this.button8.Click += new System.EventHandler(this.button8_Click);
	        // 
	        // button9
	        // 
	        this.button9.Location = new System.Drawing.Point(22, 185);
	        this.button9.Name = "button9";
	        this.button9.Size = new System.Drawing.Size(172, 23);
	        this.button9.TabIndex = 24;
	        this.button9.Text = "Slice by template...";
	        this.button9.UseVisualStyleBackColor = true;
	        this.button9.Click += new System.EventHandler(this.SliceTemplateClick);
	        // 
	        // GenerateFull
	        // 
	        this.GenerateFull.AutoSize = true;
	        this.GenerateFull.Location = new System.Drawing.Point(230, 189);
	        this.GenerateFull.Name = "GenerateFull";
	        this.GenerateFull.Size = new System.Drawing.Size(113, 17);
	        this.GenerateFull.TabIndex = 25;
	        this.GenerateFull.Text = "Generate Full Map";
	        this.GenerateFull.UseVisualStyleBackColor = true;
	        // 
	        // button10
	        // 
	        this.button10.Location = new System.Drawing.Point(530, 13);
	        this.button10.Name = "button10";
	        this.button10.Size = new System.Drawing.Size(102, 23);
	        this.button10.TabIndex = 26;
	        this.button10.Text = "Open Output";
	        this.button10.UseVisualStyleBackColor = true;
	        this.button10.Click += new System.EventHandler(this.OpenOutputClick);
	        // 
	        // Form1
	        // 
	        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
	        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
	        this.ClientSize = new System.Drawing.Size(779, 346);
	        this.Controls.Add(this.button10);
	        this.Controls.Add(this.GenerateFull);
	        this.Controls.Add(this.button9);
	        this.Controls.Add(this.button8);
	        this.Controls.Add(this.button7);
	        this.Controls.Add(this.button6);
	        this.Controls.Add(this.button5);
	        this.Controls.Add(this.GenerateFlipped);
	        this.Controls.Add(this.button4);
	        this.Controls.Add(this.button3);
	        this.Controls.Add(this.label5);
	        this.Controls.Add(this.label6);
	        this.Controls.Add(this.YStep);
	        this.Controls.Add(this.XStep);
	        this.Controls.Add(this.sPrefix);
	        this.Controls.Add(this.label3);
	        this.Controls.Add(this.label4);
	        this.Controls.Add(this.sLeftOffset);
	        this.Controls.Add(this.sTopOffset);
	        this.Controls.Add(this.label2);
	        this.Controls.Add(this.sHeight);
	        this.Controls.Add(this.label1);
	        this.Controls.Add(this.sWidth);
	        this.Controls.Add(this.button2);
	        this.Controls.Add(this.button1);
	        this.Controls.Add(this.FileName);
	        this.Name = "Form1";
	        this.Text = "Form1";
	        this.ResumeLayout(false);
	        this.PerformLayout();
        }

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox FileName;
        private System.Windows.Forms.CheckBox GenerateFlipped;
        private System.Windows.Forms.CheckBox GenerateFull;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox sHeight;
        private System.Windows.Forms.TextBox sLeftOffset;
        private System.Windows.Forms.TextBox sPrefix;
        private System.Windows.Forms.TextBox sTopOffset;
        private System.Windows.Forms.TextBox sWidth;
        private System.Windows.Forms.TextBox XStep;
        private System.Windows.Forms.TextBox YStep;

        #endregion
    }
}

